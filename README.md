dsy-notification-tools
================

Importante, dado que este es un proyecto de desarrollo, el archivo composer.json se ocupa para definición del paquete, no para la importación de dependencias.
Para eso está el archivo composer-dev.json, el cual se tiene que usar de la siguiente manera:
```
COMPOSER=composer-dev.json composer install
```
De esta manera, composer sabe que las dependencias de este proyecto están en otro archivo.
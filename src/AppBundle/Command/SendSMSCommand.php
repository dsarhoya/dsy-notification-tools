<?php

namespace AppBundle\Command;

use Aws\Exception\AwsException;
use DSYNotificationTools\Client;

use DSYNotificationTools\SMSMessage;
use AppBundle\Service\ParametersService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendSMSCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'nt:send-sms';

    /**
     * @var ParametersService
     */
    private $params;

    public function __construct(ParametersService $params)
    {
        $this->params = $params;
        parent::__construct();
    }

    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $client = new Client($this->params->awsKey, $this->params->awsSecret, 'us-east-1');
        
        $message = 'Y finalmente usando el cliente con código';
        $phone = '+56998272731';
        
        try {
            $result = $client->publishSms(new SMSMessage($message), $phone);
            dump($result);
        } catch (AwsException $e) {
            dump($e->getMessage());
        }

        $output->writeln('listo');
    }
}

<?php

namespace AppBundle\Service;

class ParametersService
{
    public $awsKey;
    public $awsSecret;

    public function __construct($awsKey, $awsSecret)
    {
        $this->awsKey = $awsKey;
        $this->awsSecret = $awsSecret;
    }
}

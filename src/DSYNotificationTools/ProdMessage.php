<?php 

namespace DSYNotificationTools;

use DSYNotificationTools\MessageInterface;

/**
 * ProdMessage class
 */
class ProdMessage implements MessageInterface
{
    const APNS = 'APNS';
    const APNS_SANDBOX = 'APNS_SANDBOX';

    /**
     * @var string
     */
    private $title;

    /**
     * @var array
     */
    private $payload;

    /**
     * @var array
     */
    private $apsPayload;

    /**
     * @var bool
     */
    private $addCapacitor = false;

    /**
     * Constructor
     *
     * @param string $title
     * @param array $payload
     * @param array $apsPayload
     */
    public function __construct(string $title, array $payload = [], array $apsPayload = [])
    {
        $this->title = $title;
        $this->payload = $payload;
        $this->apsPayload = $apsPayload;
    }

    public function addCapacitor(bool $addCapacitor): self
    {
        $this->addCapacitor = $addCapacitor;
        return $this;
    }

    /**
     * assemble function
     *
     * @return string JSON the assembled message
     */
    public function assemble()
    {
        $message = [
            "default" => "SNS message", //Qué significa la llave default?
            "GCM" => $this->getSNSNotificationMessage('android')
        ];

        $message[$this->getEnvironmentName()] = $this->getSNSNotificationMessage('ios');

        return json_encode($message);
    }

    /**
     * getSNSNotificationMessage function
     *
     * @param string $platform
     * @return string JSON
     */
    private function getSNSNotificationMessage($platform = 'ios')
    {
        $data = [];
        $payload = $this->payload;
        $payload = is_array($payload) ? $payload : [];

        if ('ios' === $platform) {
            // $data['aps'] = ['alert' => $this->title];
            $data = array_merge([
                'aps' => array_merge(['alert' => $this->title], $this->apsPayload)
            ], $payload);
        } else {
            $data['data'] = array_merge([
                'message' => $this->title
            ], $payload);
        }

        if (true === $this->addCapacitor) {
            $data['notification'] = [
                'title' => $this->title
            ];
        }

        return json_encode($data);
    }

    /**
     * getEnvironmentName function
     *
     * @return string
     */
    protected function getEnvironmentName()
    {
        return self::APNS;
    }
}

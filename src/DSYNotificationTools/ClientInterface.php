<?php 

namespace DSYNotificationTools;

use DSYNotificationTools\MessageInterface;

interface ClientInterface
{
    public function publish(MessageInterface $message, $destination);
    public function createPlatformEndpoint($platform, $token);
}

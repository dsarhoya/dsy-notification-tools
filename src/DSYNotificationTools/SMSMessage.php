<?php 

namespace DSYNotificationTools;

use DSYNotificationTools\MessageInterface;

/**
 * 
 */
class SMSMessage implements MessageInterface
{

    CONST SMS_TYPE_PROMOTIONAL = 'Promotional';
    CONST SMS_TYPE_TRANSACTIONAL = 'Transactional';
    
    private $message;
    private $options;
    
    function __construct($message, array $options = [])
    {
        $this->message = $message;
        $this->options = array_replace([
            'senderId' => null,
            'smsType' => self::SMS_TYPE_PROMOTIONAL,
        ], $options);
    }
    
    /**
     * @return array the assembled message
     */
    public function assemble(){
        $res = [
            'Message' => $this->message,
            'MessageStructure' => 'string',
            'MessageAttribute' => [
                'AWS.SNS.SMS.SMSType'  => [
                    'DataType'    => 'String',
                    'StringValue' => $this->options['smsType'],
                ]
            ]
        ];
                
        if (is_string($this->options['senderId'])) {
            $res['MessageAttribute']['AWS.SNS.SMS.SenderID'] = [
                'DataType'    => 'String',
                'StringValue' => $this->options['senderId'],
            ];
        }

        return $res;

    }
        
}

<?php 

namespace DSYNotificationTools;

interface MessageInterface
{
    /**
     * @return array the assembled message
     */
    public function assemble();
}

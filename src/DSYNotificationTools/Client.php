<?php

namespace DSYNotificationTools;

use Aws\Sns\SnsClient;
use Aws\Sns\Exception\SnsException;
use DSYNotificationTools\SMSMessage;
use DSYNotificationTools\ClientInterface;
use DSYNotificationTools\MessageInterface;

/**
 * 
 */
class Client implements ClientInterface
{
    
    private $key;
    private $secret;
    private $region;
    
    function __construct($key, $secret, $region)
    {
        $this->key = $key;
        $this->secret = $secret;
        $this->region = $region;
    }
    
    /**
     * Devuelve el cliente en caso de necesitarlo
     * @return SnsClient Cliente SNS de AWS
     */
    public function getClient(){
        return new SnsClient([
            "version"     => 'latest',
            "region"=>$this->region,
            "credentials" => [
                "key"    => $this->key,
                "secret" => $this->secret,
            ],
        ]);
    }
    
    /**
     * publish a message to a destination
     * @param  MessageInterface $message the text of the notification
     * @param  $destination string representing the destination
     * @return null no return for now
     */
    public function publish(MessageInterface $message, $destination){
        try {
            $res = $this->getClient()->publish([
                "Message"=>$message->assemble(),
                "TargetArn"=>$destination,
                "MessageStructure"=>"json"
            ]);
        } catch (SnsException $ex) {
            return false;
        }
        
        if(isset($res['@metadata']) && $res['@metadata']['statusCode'] !== 200){
            return false;
        }
        
        return true;

    }
    
    /**
     * publish a message to a destination
     * @param  SMSMessage $message configuration
     * @param  $destination string representing the mobile phone number
     * @return null no return for now
     */
    public function publishSms(SMSMessage $message, $destination){

        $assembled = $message->assemble();
        $assembled['PhoneNumber'] = $destination;
        
        try {
            $res = $this->getClient()->publish($assembled);
        } catch (SnsException $ex) {
            return false;
        }
        
        if(isset($res['@metadata']) && $res['@metadata']['statusCode'] !== 200){
            return false;
        }
        
        return true;
    }
    
    /**
     * Crea un enpoint asociado a una plataforma
     * @param  string $platform 
     * @param  string $token
     * @return string el arn
     */
    public function createPlatformEndpoint($platform, $token){
        $result = $this->getClient()->createPlatformEndpoint([
            'PlatformApplicationArn' => $platform,
            'Token' => $token,
        ]);
        return $result["EndpointArn"];
    }
}

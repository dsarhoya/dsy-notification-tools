<?php 

namespace DSYNotificationTools;

use DSYNotificationTools\ProdMessage;

/**
 * 
 */
class DevMessage extends ProdMessage
{
    protected function getEnvironmentName(){
        return self::APNS_SANDBOX;
    }
}
